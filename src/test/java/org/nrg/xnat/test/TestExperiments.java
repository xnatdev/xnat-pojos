package org.nrg.xnat.test;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.enums.Month;
import org.nrg.xnat.pojo.DataType;
import org.nrg.xnat.pojo.experiments.ImagingSession;
import org.nrg.xnat.pojo.experiments.Scan;
import org.nrg.xnat.pojo.experiments.sessions.MRSession;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.AssertJUnit.*;

public class TestExperiments {

    @Test
    public void testImagingSession() {
        final ImagingSession genericSession = new ImagingSession();
        assertTrue(StringUtils.isNotEmpty(genericSession.getLabel()));
    }

    @Test
    public void testMRSession() {
        final Month month = Month.MARCH;
        final int day = 12;
        final int year = 2010;
        final Scan scan = new Scan().id("scan1");

        final MRSession mrSession = new MRSession().month(month).day(day).year(year);
        mrSession.addScan(scan);

        assertEquals(month, mrSession.getMonth());
        assertEquals(day, mrSession.getDay());
        assertEquals(year, mrSession.getYear());
        assertEquals(Collections.singletonList(scan), mrSession.getScans());
        assertEquals(DataType.MR_SESSION, mrSession.getDataType());
    }
}
