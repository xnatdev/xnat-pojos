package org.nrg.xnat.test;

import org.nrg.xnat.enums.Gender;
import org.nrg.xnat.enums.Handedness;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Share;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.ImagingSession;
import org.nrg.xnat.pojo.experiments.NonimagingAssessor;
import org.nrg.xnat.pojo.experiments.SubjectAssessor;
import org.nrg.xnat.pojo.resources.Resource;
import org.nrg.xnat.pojo.resources.SubjectResource;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.AssertJUnit.*;

public class TestSubject {

    @Test
    public void testDefaults() {
        final Gender gender = Gender.UNKNOWN;
        final Handedness handedness = Handedness.UNKNOWN;
        final Project project = new Project();

        final Subject subject = new Subject(project);
        final String generatedLabel = subject.getLabel();

        assertNotNull(generatedLabel);
        assertEquals(gender, subject.getGender());
        assertEquals(handedness, subject.getHandedness());
    }

    @Test
    public void testFullSubject() {
        final Project project = new Project();
        final Project otherProject = new Project();
        final Project otherProject2 = new Project();

        final String label = "test_subjectA";
        final Gender gender = Gender.MALE;
        final int yob = 1990;
        final Handedness handedness = Handedness.LEFT;
        final String race = "Indianapolis 500";
        final String ethnicity = "Not latino";
        final int height = 72;
        final int weight = 200;
        final String src = "src";
        final Subject subject = new Subject(project, label).gender(gender).yob(yob).handedness(handedness).
                race(race).ethnicity(ethnicity).height(height).weight(weight).src(src);

        assertEquals(label, subject.getLabel());
        assertEquals(gender, subject.getGender());
        assertEquals(yob, subject.getYob());
        assertEquals(handedness, subject.getHandedness());
        assertEquals(race, subject.getRace());
        assertEquals(ethnicity, subject.getEthnicity());
        assertEquals(height, subject.getHeight());
        assertEquals(weight, subject.getWeight());
        assertEquals(src, subject.getSrc());

        final Share share1 = new Share(otherProject.getId(), "new_label");
        final Share share2 = new Share(otherProject2.getId(), "new_label2");
        final List<Share> shares = Arrays.asList(share1, share2);
        final Resource resource = new SubjectResource(project, subject, "resource");
        final List<Resource> resources = Collections.singletonList(resource);
        final SubjectAssessor session = new ImagingSession(project, subject);
        final SubjectAssessor assessor = new NonimagingAssessor(project, subject, "form1");
        final List<SubjectAssessor> subjectAssessors = Arrays.asList(session, assessor);
        subject.shares(shares).resources(resources).experiments(subjectAssessors);

        assertEquals(shares, subject.getShares());
        assertEquals(resources, subject.getResources());
        assertEquals(subjectAssessors, subject.getExperiments());
    }
}
