package org.nrg.xnat.test;

import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.pojo.Investigator;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.User;
import org.nrg.xnat.pojo.resources.ProjectResource;
import org.nrg.xnat.pojo.resources.Resource;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.AssertJUnit.*;

public class TestProject {

    @Test
    public void testDefaults() {
        final Project project = new Project();
        final String generatedId = project.getId();

        assertNotNull(generatedId);
        assertEquals(generatedId, project.getRunningTitle());
        assertEquals(generatedId, project.getTitle());
        assertEquals(Accessibility.PRIVATE, project.getAccessibility());
    }

    @Test
    public void testFullProject() {
        final Investigator investigator1 = new Investigator().firstname("Test").lastname("User");
        final Investigator investigator2 = new Investigator().firstname("Other").lastname("Person");
        final User userA = new User("userA");
        final User userB = new User("userB");
        final User userC = new User("userC");
        final User userD = new User("userD");

        final String id = "test_project001";
        final String runningTitle = "Test Study 001";
        final String title = "XNAT POJO Test Study 001";
        final String description = "This is part of a unit test for xnat-pojos.";
        final List<String> keywords = Arrays.asList("key1", "key2");
        final List<String> aliases = Arrays.asList("altName1", "altNameA");
        final Investigator pi = investigator1;
        final List<Investigator> investigators = Collections.singletonList(investigator2);
        final List<User> owners = Collections.singletonList(userA);
        final List<User> members = Collections.singletonList(userB);
        final List<User> collaborators = Arrays.asList(userC, userD);
        final Accessibility accessibility = Accessibility.PUBLIC;

        final Project project = new Project(id).runningTitle(runningTitle).title(title).description(description).keywords(keywords).aliases(aliases).
                pi(pi).investigators(investigators).owners(owners).members(members).collaborators(collaborators).accessibility(accessibility);
        assertEquals(id, project.getId());
        assertEquals(runningTitle, project.getRunningTitle());
        assertEquals(title, project.getTitle());
        assertEquals(description, project.getDescription());
        assertEquals(keywords, project.getKeywords());
        assertEquals(aliases, project.getAliases());
        assertEquals(pi, project.getPi());
        assertEquals(investigators, project.getInvestigators());
        assertEquals(owners, project.getOwners());
        assertEquals(members, project.getMembers());
        assertEquals(collaborators, project.getCollaborators());
        assertEquals(accessibility, project.getAccessibility());
        assertTrue(project.getSubjects().isEmpty());
        assertTrue(project.getProjectResources().isEmpty());

        final Subject subjectA = new Subject(project, "testSubjectA");
        final Subject subjectB = new Subject(project, "testSubjectB");
        final Subject subjectC = new Subject(project, "testSubjectC");
        final List<Subject> subjects = Arrays.asList(subjectA, subjectB, subjectC);
        final Resource resourceA = new ProjectResource(project, "resourceA");
        final Resource resourceB = new ProjectResource(project, "resourceB");
        final List<Resource> resources = Arrays.asList(resourceA, resourceB);

        project.resources(Arrays.asList(resourceA, resourceB)).subjects(Arrays.asList(subjectA, subjectB, subjectC));
        assertEquals(subjects, project.getSubjects());
        assertEquals(resources, project.getProjectResources());
    }

}
