package org.nrg.xnat.enums;

public enum DicomEditVersion {
    DE_4, DE_6, UNSPECIFIED
}
