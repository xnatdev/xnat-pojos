package org.nrg.xnat.enums;

import org.apache.commons.lang3.StringUtils;

public enum Handedness {
    RIGHT ("right"),
    LEFT ("left"),
    AMBIDEXTROUS ("ambidextrous"),
    UNKNOWN ("unknown");

    private final String string;

    Handedness(String string) {
        this.string = string;
    }

    public static Handedness get(String value) {
        for (Handedness handedness : values()) {
            if (StringUtils.equalsIgnoreCase(handedness.string, value)) return handedness;
        }
        return null;
    }

    public String capitalize() {
        return StringUtils.capitalize(string);
    }

    @Override
    public String toString() {
        return string;
    }
}
