package org.nrg.xnat.enums;

import org.apache.commons.lang3.StringUtils;

public enum ScriptLocation {
    FILE ("file"),
    URL  ("url");

    private final String location;

    ScriptLocation(String location) {
        this.location = location;
    }

    public static ScriptLocation get(String location) {
        for (ScriptLocation possibleLocation : values()) {
            if (StringUtils.equalsIgnoreCase(possibleLocation.location, location)) {
                return possibleLocation;
            }
        }
        return null;
    }
}
