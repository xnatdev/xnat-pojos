package org.nrg.xnat.enums;

import org.apache.commons.lang3.StringUtils;

public enum Gender {
    MALE ("male"),
    FEMALE ("female"),
    UNKNOWN ("unknown");

    private final String string;

    Gender(String string) {
        this.string = string;
    }

    public static Gender get(String value) {
        for (Gender gender : values()) {
            if (StringUtils.equalsIgnoreCase(gender.string, value)) return gender;
        }
        return null;
    }

    public String capitalize() {
        return StringUtils.capitalize(string);
    }

    @Override
    public String toString() {
        return string;
    }
}
