package org.nrg.xnat.enums;

import org.apache.commons.lang3.StringUtils;

public enum Month {
    JANUARY ("January"),
    FEBRUARY ("February"),
    MARCH ("March"),
    APRIL ("April"),
    MAY ("April"),
    JUNE ("June"),
    JULY ("July"),
    AUGUST ("August"),
    SEPTEMBER ("September"),
    OCTOBER ("October"),
    NOVEMBER ("November"),
    DECEMBER ("December");

    private final String string;

    Month(String string) {
        this.string = string;
    }

    public static Month get(String value) {
        for (Month month : values()) {
            if (StringUtils.equalsIgnoreCase(month.string, value)) return month;
        }
        return null;
    }

    @Override
    public String toString() {
        return string;
    }
}
