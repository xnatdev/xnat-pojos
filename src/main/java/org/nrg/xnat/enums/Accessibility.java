package org.nrg.xnat.enums;

import org.apache.commons.lang3.StringUtils;

public enum Accessibility {
    PUBLIC ("public"),
    PROTECTED ("protected"),
    PRIVATE ("private");

    private final String string;

    Accessibility(String string) {
        this.string = string;
    }

    public static Accessibility get(String value) {
        for (Accessibility accessibility : values()) {
            if (StringUtils.equalsIgnoreCase(accessibility.string, value)) return accessibility;
        }
        return null;
    }

    @Override
    public String toString() {
        return string;
    }
}
