package org.nrg.xnat.util;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomUtils {

    private static final int DEFAULT_ID_LENGTH = 10;

    public static String randomID(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public static String randomID() {
        return randomID(DEFAULT_ID_LENGTH);
    }

}
