package org.nrg.xnat.util;

import java.util.List;

public class ListUtils {

    public static <X> void copyInto(List<X> from, List<X> into) {
        into.clear();
        if (from != null && !from.isEmpty()) into.addAll(from);
    }

}
