package org.nrg.xnat.util;

import java.util.Map;

public class MapUtils {

    public static <T, V> void copyInto(Map<T, V> from, Map<T, V> into) {
        into.clear();
        if (from != null && !from.isEmpty()) into.putAll(from);
    }

}
