package org.nrg.xnat.pojo;

import org.nrg.xnat.Extension;

public class Share {
    private String destinationProject;
    private String destinationLabel;
    private Extension<Share> extension;

    public Share(String project, String label) {
        destinationProject = project;
        destinationLabel = label;
    }

    public Share() {}

    public String getDestinationProject() {
        return destinationProject;
    }

    public void setDestinationProject(String destinationProject) {
        this.destinationProject = destinationProject;
    }

    public String getDestinationLabel() {
        return destinationLabel;
    }

    public void setDestinationLabel(String destinationLabel) {
        this.destinationLabel = destinationLabel;
    }

    public Extension<Share> getExtension() {
        return extension;
    }

    public void setExtension(Extension<Share> extension) {
        this.extension = extension;
    }

    public Share destinationProject(String project) {
        setDestinationProject(project);
        return this;
    }

    public Share destinationLabel(String label) {
        setDestinationLabel(label);
        return this;
    }

    public Share extension(Extension<Share> extension) {
        setExtension(extension);
        return this;
    }
}
