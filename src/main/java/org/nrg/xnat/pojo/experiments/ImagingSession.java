package org.nrg.xnat.pojo.experiments;

import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.util.ListUtils;
import org.nrg.xnat.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;

public class ImagingSession extends SubjectAssessor {

    protected final List<Scan> scans = new ArrayList<>();
    protected final List<SessionAssessor> assessors = new ArrayList<>();

    public ImagingSession(Project project, Subject subject, String label) {
        this.primaryProject = project;
        this.subject = subject;
        this.label = (label != null) ? label : RandomUtils.randomID();
    }

    public ImagingSession(Project project, Subject subject) {
        this(project, subject, null);
    }

    public ImagingSession(String label) {
        this(null, null, label);
    }

    public ImagingSession() {
        this(null, null, null);
    }

    public List<Scan> getScans() {
        return scans;
    }

    public void setScans(List<Scan> scans) {
        ListUtils.copyInto(scans, this.scans);
    }

    public List<SessionAssessor> getAssessors() {
        return assessors;
    }

    public void setAssessors(List<SessionAssessor> assessors) {
        ListUtils.copyInto(assessors, this.assessors);
    }

    @SuppressWarnings("unchecked")
    public <T extends ImagingSession> T scans(List<Scan> scans) {
        ListUtils.copyInto(scans, this.scans);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends ImagingSession> T addScan(Scan scan) {
        scans.add(scan);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends ImagingSession> T assessors(List<SessionAssessor> assessors) {
        ListUtils.copyInto(assessors, this.assessors);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends ImagingSession> T addAssessor(SessionAssessor assessor) {
        assessors.add(assessor);
        return (T)this;
    }

}
