package org.nrg.xnat.pojo.experiments.sessions;

import org.nrg.xnat.pojo.DataType;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.ImagingSession;

public class PETMRSession extends ImagingSession {

    private String tracer;

    public PETMRSession(Project project, Subject subject, String label) {
        super(project, subject, label);
        setDataType(DataType.PET_MR_SESSION);
    }

    public PETMRSession(Project project, Subject subject) {
        this(project, subject, null);
    }

    public PETMRSession(Project project, String label) {
        this(project, null, label);
    }

    public PETMRSession(String label) {
        this(null, null, label);
    }

    public PETMRSession() {
        this(null, (Subject)null);
    }

    public String getTracer() {
        return tracer;
    }

    public void setTracer(String tracer) {
        this.tracer = tracer;
    }

    public PETMRSession tracer(String tracer) {
        setTracer(tracer);
        return this;
    }

}
