package org.nrg.xnat.pojo.experiments;

import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;

public class SessionAssessor extends Experiment {

    private ImagingSession parentSession;

    public SessionAssessor(Project project, Subject subject, ImagingSession parentSession, String label) {
        this.primaryProject = project;
        this.subject = subject;
        this.parentSession = parentSession;
        this.label = label;
    }

    public SessionAssessor() {}

    public ImagingSession getParentSession() {
        return parentSession;
    }

    public void setParentSession(ImagingSession parentSession) {
        this.parentSession = parentSession;
    }

    @SuppressWarnings("unchecked")
    public <T extends SessionAssessor> T parentSession(ImagingSession parentSession) {
        setParentSession(parentSession);
        return (T)this;
    }

}
