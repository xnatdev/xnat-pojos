package org.nrg.xnat.pojo.experiments;

import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;

public class NonimagingAssessor extends SubjectAssessor {

    public NonimagingAssessor(Project project, Subject subject, String label) {
        this.primaryProject = project;
        this.subject = subject;
        this.label = label;
    }

    public NonimagingAssessor() {}

}
