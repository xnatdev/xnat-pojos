package org.nrg.xnat.pojo.experiments.sessions;

import org.nrg.xnat.pojo.DataType;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.ImagingSession;

public class MRSession extends ImagingSession {

    public MRSession(Project project, Subject subject, String label) {
        super(project, subject, label);
        setDataType(DataType.MR_SESSION);
    }

    public MRSession(Project project, Subject subject) {
        this(project, subject, null);
    }

    public MRSession(Project project, String label) {
        this(project, null, label);
    }

    public MRSession(String label) {
        this(null, null, label);
    }

    public MRSession() {
        this(null, (Subject)null);
    }
    
}
