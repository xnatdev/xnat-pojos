package org.nrg.xnat.pojo.experiments;

import org.nrg.xnat.Extension;
import org.nrg.xnat.enums.Month;
import org.nrg.xnat.pojo.DataType;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Share;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.custom_variable.CustomVariableContainer;
import org.nrg.xnat.pojo.resources.Resource;
import org.nrg.xnat.util.ListUtils;
import org.nrg.xnat.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class Experiment extends CustomVariableContainer {

    protected Project primaryProject;
    protected Subject subject;
    protected String label;
    protected DataType dataType;
    protected Month month;
    protected int day;
    protected int year;
    protected final List<Resource> resources = new ArrayList<>();
    protected final List<Share> shares = new ArrayList<>();
    protected Extension<? extends Experiment> extension;

    public Project getPrimaryProject() {
        return primaryProject;
    }

    public void setPrimaryProject(Project primaryProject) {
        this.primaryProject = primaryProject;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        ListUtils.copyInto(resources, this.resources);
    }

    public List<Share> getShares() {
        return shares;
    }

    public void setShares(List<Share> shares) {
        ListUtils.copyInto(shares, this.shares);
    }

    public Extension<? extends Experiment> getExtension() {
        return extension;
    }

    public void setExtension(Extension<? extends Experiment> extension) {
        this.extension = extension;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T project(Project project) {
        setPrimaryProject(project);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T subject(Subject subject) {
        setSubject(subject);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T label(String label) {
        setLabel(label);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T dataType(DataType dataType) {
        setDataType(dataType);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T month(Month month) {
        setMonth(month);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T day(int day) {
        setDay(day);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T year(int year) {
        setYear(year);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T resources(List<Resource> resources) {
        setResources(resources);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T addResource(Resource resource) {
        resources.add(resource);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T shares(List<Share> shares) {
        setShares(shares);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T addShare(Share share) {
        shares.add(share);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public <T extends Experiment> T extension(Extension<? extends Experiment> extension) {
        setExtension(extension);
        return (T)this;
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Experiment)) return false;

        Experiment that = (Experiment) o;

        if (primaryProject != null ? !primaryProject.equals(that.primaryProject) : that.primaryProject != null)
            return false;
        return label != null ? label.equals(that.label) : that.label == null;
    }

    @Override
    public int hashCode() {
        int result = primaryProject != null ? primaryProject.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }

}
