package org.nrg.xnat.pojo.experiments;

import org.nrg.xnat.Extension;
import org.nrg.xnat.pojo.resources.Resource;
import org.nrg.xnat.util.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class Scan {

    private ImagingSession session;
    private String id;
    private String xsiType;
    private String seriesDescription;
    private String type;
    private String note;
    private String quality = "usable";
    private final List<Resource> scanResources = new ArrayList<>();
    private Extension<Scan> extension;

    public Scan(ImagingSession session, String id) {
        this.session = session;
        this.id = id;
    }

    public Scan() {}

    public ImagingSession getSession() {
        return session;
    }

    public void setSession(ImagingSession session) {
        this.session = session;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getXsiType() {
        return xsiType;
    }

    public void setXsiType(String xsiType) {
        this.xsiType = xsiType;
    }

    public String getSeriesDescription() {
        return seriesDescription;
    }

    public void setSeriesDescription(String seriesDescription) {
        this.seriesDescription = seriesDescription;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public List<Resource> getScanResources() {
        return scanResources;
    }

    public void setScanResources(List<Resource> scanResources) {
        ListUtils.copyInto(scanResources, this.scanResources);
    }

    public Extension<Scan> getExtension() {
        return extension;
    }

    public void setExtension(Extension<Scan> extension) {
        this.extension = extension;
    }

    public Scan session(ImagingSession session) {
        setSession(session);
        return this;
    }

    public Scan id(String id) {
        setId(id);
        return this;
    }

    public Scan xsiType(String xsiType) {
        setXsiType(xsiType);
        return this;
    }

    public Scan seriesDescription(String seriesDescription) {
        setSeriesDescription(seriesDescription);
        return this;
    }

    public Scan type(String type) {
        setType(type);
        return this;
    }

    public Scan note(String note) {
        setNote(note);
        return this;
    }

    public Scan quality(String quality) {
        setQuality(quality);
        return this;
    }

    public Scan scanResources(List<Resource> scanResources) {
        setScanResources(scanResources);
        return this;
    }

    public Scan addResource(Resource resource) {
        scanResources.add(resource);
        return this;
    }

    public Scan extension(Extension<Scan> extension) {
        setExtension(extension);
        return this;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Scan scan = (Scan) o;

        return id != null ? id.equals(scan.id) : scan.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
