package org.nrg.xnat.pojo.experiments.sessions;

import org.nrg.xnat.pojo.DataType;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.ImagingSession;
import org.nrg.xnat.util.RandomUtils;

public class CTSession extends ImagingSession {

    public CTSession(Project project, Subject subject, String label) {
        super(project, subject, label);
        setDataType(DataType.CT_SESSION);
    }

    public CTSession(Project project, Subject subject) {
        this(project, subject, null);
    }

    public CTSession(Project project, String label) {
        this(project, null, label);
    }

    public CTSession(String label) {
        this(null, null, label);
    }

    public CTSession() {
        this(null, (Subject)null);
    }
    
}
