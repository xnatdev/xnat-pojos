package org.nrg.xnat.pojo.experiments.sessions;

import org.nrg.xnat.pojo.DataType;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.ImagingSession;

public class PETSession extends ImagingSession {

    private String tracer;

    public PETSession(Project project, Subject subject, String label) {
        super(project, subject, label);
        setDataType(DataType.PET_SESSION);
    }

    public PETSession(Project project, Subject subject) {
        this(project, subject, null);
    }

    public PETSession(Project project, String label) {
        this(project, null, label);
    }

    public PETSession(String label) {
        this(null, null, label);
    }

    public PETSession() {
        this(null, (Subject)null);
    }

    public String getTracer() {
        return tracer;
    }

    public void setTracer(String tracer) {
        this.tracer = tracer;
    }

    public PETSession tracer(String tracer) {
        setTracer(tracer);
        return this;
    }

}
