package org.nrg.xnat.pojo.extensions;

import org.nrg.xnat.Extension;
import org.nrg.xnat.enums.ScriptLocation;
import org.nrg.xnat.pojo.AnonScript;

public abstract class AnonScriptExtension implements Extension<AnonScript> {

    private ScriptLocation location;
    private String locationData;
    AnonScript script;

    public abstract String readScriptFromFile();

    public abstract String readScriptFromURL();

    public AnonScriptExtension(AnonScript script, ScriptLocation location, String locationData) {
        this.script = script;
        script.extension(this);
        this.location = location;
        this.locationData = locationData;
    }

    public AnonScriptExtension(AnonScript script, String location, String locationData) {
        this(script, ScriptLocation.get(location), locationData);
    }

    public AnonScriptExtension() {}

    public ScriptLocation getLocation() {
        return location;
    }

    public void setLocation(ScriptLocation location) {
        this.location = location;
    }

    public String getLocationData() {
        return locationData;
    }

    public void setLocationData(String locationData) {
        this.locationData = locationData;
    }

    public AnonScript getScript() {
        return script;
    }

    public void setScript(AnonScript script) {
        this.script = script;
    }

    public String readScript() {
        switch (location) {
            case FILE:
                return readScriptFromFile();
            case URL:
                return readScriptFromURL();
        }
        return null;
    }

}
