package org.nrg.xnat.pojo;

import org.nrg.xnat.Extension;

public class User {

    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private boolean verified;
    private boolean enabled;
    private boolean admin = false;
    private Extension<User> extension;

    public User(String username) {
        this.username = username;
    }

    public User() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Extension<User> getExtension() {
        return extension;
    }

    public void setExtension(Extension<User> extension) {
        this.extension = extension;
    }

    public User username(String username) {
        setUsername(username);
        return this;
    }

    public User password(String password) {
        setPassword(password);
        return this;
    }

    public User email(String email) {
        setEmail(email);
        return this;
    }

    public User firstName(String firstName) {
        setFirstName(firstName);
        return this;
    }

    public User lastName(String lastName) {
        setLastName(lastName);
        return this;
    }

    public User verified(boolean verified) {
        setVerified(verified);
        return this;
    }

    public User enabled(boolean enabled) {
        setEnabled(enabled);
        return this;
    }

    public User admin(boolean admin) {
        setAdmin(admin);
        return this;
    }

    public User extension(Extension<User> extension) {
        setExtension(extension);
        return this;
    }

    @Override
    public String toString() {
        return String.format("%s, %s (%s)", lastName, firstName, username);
    }
}
