package org.nrg.xnat.pojo.resources;

public class ScanResource extends Resource {

    @Override
    public String resourceUrl() {
        return String.format("data/projects/%s/subjects/%s/experiments/%s/scans/%s", project, subject, subjectAssessor, scan);
    }

}
