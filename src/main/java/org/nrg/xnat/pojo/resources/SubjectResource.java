package org.nrg.xnat.pojo.resources;

import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;

public class SubjectResource extends Resource {

    @Override
    public String resourceUrl() {
        return String.format("data/projects/%s/subjects/%s", project, subject);
    }

    public SubjectResource(Project project, Subject subject, String label) {
        setProject(project);
        setSubject(subject);
        setFolder(label);
    }

    public SubjectResource() {
        super();
    }

}
