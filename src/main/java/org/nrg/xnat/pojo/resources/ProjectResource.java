package org.nrg.xnat.pojo.resources;

import org.nrg.xnat.pojo.Project;

public class ProjectResource extends Resource {

    @Override
    public String resourceUrl() {
        return String.format("data/projects/%s", project);
    }

    public ProjectResource(Project project, String label) {
        setProject(project);
        setFolder(label);
    }

    public ProjectResource() {
        super();
    }

}
