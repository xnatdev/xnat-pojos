package org.nrg.xnat.pojo.resources;

import org.nrg.xnat.Extension;

public class ResourceFile {

    private String name;
    private Resource resourceFolder;
    private boolean unzip = false;
    private String content;
    private Extension<ResourceFile> extension;

    public ResourceFile(Resource resourceFolder, String name) {
        this.resourceFolder = resourceFolder;
        this.name = name;
    }

    public ResourceFile() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Resource getResourceFolder() {
        return resourceFolder;
    }

    public void setResourceFolder(Resource resourceFolder) {
        this.resourceFolder = resourceFolder;
    }

    public boolean isUnzip() {
        return unzip;
    }

    public void setUnzip(boolean unzip) {
        this.unzip = unzip;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Extension<ResourceFile> getExtension() {
        return extension;
    }

    public void setExtension(Extension<ResourceFile> extension) {
        this.extension = extension;
    }

    public ResourceFile name(String name) {
        setName(name);
        return this;
    }

    public ResourceFile resourceFolder(Resource resourceFolder) {
        setResourceFolder(resourceFolder);
        return this;
    }

    public ResourceFile unzip(boolean unzip) {
        setUnzip(unzip);
        return this;
    }

    public ResourceFile content(String content) {
        setContent(content);
        return this;
    }

    public ResourceFile extension(Extension<ResourceFile> extension) {
        setExtension(extension);
        return this;
    }
}
