package org.nrg.xnat.pojo.resources;

import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.SubjectAssessor;

public class SubjectAssessorResource extends Resource {

    @Override
    public String resourceUrl() {
        return String.format("data/projects/%s/subjects/%s/experiments/%s", project, subject, subjectAssessor);
    }

    public SubjectAssessorResource(Project project, Subject subject, SubjectAssessor subjectAssessor, String label) {
        setProject(project);
        setSubject(subject);
        setSubjectAssessor(subjectAssessor);
        setFolder(label);
    }

    public SubjectAssessorResource() {
        super();
    }

}
