package org.nrg.xnat.pojo.resources;

public class SessionAssessorResource extends Resource {

    @Override
    public String resourceUrl() {
        return String.format("data/projects/%s/subjects/%s/experiments/%s/assessors/%s", project, subject, subjectAssessor, sessionAssessor);
    }

}
