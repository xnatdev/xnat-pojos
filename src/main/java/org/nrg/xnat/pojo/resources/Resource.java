package org.nrg.xnat.pojo.resources;

import org.nrg.xnat.Extension;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.Scan;
import org.nrg.xnat.pojo.experiments.SessionAssessor;
import org.nrg.xnat.pojo.experiments.SubjectAssessor;
import org.nrg.xnat.util.ListUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class Resource {
    protected Project project;
    protected Subject subject;
    protected SubjectAssessor subjectAssessor;
    protected Scan scan;
    protected SessionAssessor sessionAssessor;
    protected String folder;
    protected final List<ResourceFile> resourceFiles = new ArrayList<>();
    protected Extension<Resource> extension;

    public abstract String resourceUrl();

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public SubjectAssessor getSubjectAssessor() {
        return subjectAssessor;
    }

    public void setSubjectAssessor(SubjectAssessor subjectAssessor) {
        this.subjectAssessor = subjectAssessor;
    }

    public Scan getScan() {
        return scan;
    }

    public void setScan(Scan scan) {
        this.scan = scan;
    }

    public SessionAssessor getSessionAssessor() {
        return sessionAssessor;
    }

    public void setSessionAssessor(SessionAssessor sessionAssessor) {
        this.sessionAssessor = sessionAssessor;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public List<ResourceFile> getResourceFiles() {
        return resourceFiles;
    }

    public void setResourceFiles(List<ResourceFile> resourceFiles) {
        ListUtils.copyInto(resourceFiles, this.resourceFiles);
    }

    public Extension<Resource> getExtension() {
        return extension;
    }

    public void setExtension(Extension<Resource> extension) {
        this.extension = extension;
    }

    public Resource project(Project project) {
        setProject(project);
        return this;
    }

    public Resource subject(Subject subject) {
        setSubject(subject);
        return this;
    }

    public Resource subjectAssessor(SubjectAssessor subjectAssessor) {
        setSubjectAssessor(subjectAssessor);
        return this;
    }

    public Resource scan(Scan scan) {
        setScan(scan);
        return this;
    }

    public Resource sessionAssessor(SessionAssessor sessionAssessor) {
        setSessionAssessor(sessionAssessor);
        return this;
    }

    public Resource folder(String label) {
        setFolder(label);
        return this;
    }

    public Resource resourceFiles(List<ResourceFile> resourceFiles) {
        setResourceFiles(resourceFiles);
        return this;
    }

    public Resource addResourceFile(ResourceFile resourceFile) {
        resourceFiles.add(resourceFile);
        return this;
    }

    public Resource extension(Extension<Resource> extension) {
        setExtension(extension);
        return this;
    }
}
