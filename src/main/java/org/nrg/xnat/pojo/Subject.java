package org.nrg.xnat.pojo;

import org.nrg.xnat.Extension;
import org.nrg.xnat.enums.Gender;
import org.nrg.xnat.enums.Handedness;
import org.nrg.xnat.pojo.custom_variable.CustomVariableContainer;
import org.nrg.xnat.pojo.experiments.SubjectAssessor;
import org.nrg.xnat.pojo.resources.Resource;
import org.nrg.xnat.util.ListUtils;
import org.nrg.xnat.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;

public class Subject extends CustomVariableContainer {

    private Project project;
    private String label;
    private Gender gender = Gender.UNKNOWN;
    private int yob;
    private int age;
    private String dob;
    private Handedness handedness = Handedness.UNKNOWN;
    private String race;
    private String ethnicity;
    private int height;
    private int weight;
    private String src;
    private int education;
    private String group;
    private final List<Share> shares = new ArrayList<>();
    private final List<Resource> resources = new ArrayList<>();
    private final List<SubjectAssessor> experiments = new ArrayList<>();
    private Extension<Subject> extension;

    public Subject(Project project, String label) {
        this.project = project;
        this.label = label;
    }

    public Subject(Project project) {
        this(project, RandomUtils.randomID());
    }

    public Subject() {
        this(null);
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setGender(String gender) {
        setGender(Gender.get(gender));
    }

    public int getYob() {
        return yob;
    }

    public void setYob(int yob) {
        this.yob = yob;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Handedness getHandedness() {
        return handedness;
    }

    public void setHandedness(Handedness handedness) {
        this.handedness = handedness;
    }

    public void setHandedness(String handedness) {
        setHandedness(Handedness.get(handedness));
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public int getEducation() {
        return education;
    }

    public void setEducation(int education) {
        this.education = education;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public List<Share> getShares() {
        return shares;
    }

    public void setShares(List<Share> shares) {
        ListUtils.copyInto(shares, this.shares);
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        ListUtils.copyInto(resources, this.resources);
    }

    public List<SubjectAssessor> getExperiments() {
        return experiments;
    }

    public void setExperiments(List<SubjectAssessor> experiments) {
        ListUtils.copyInto(experiments, this.experiments);
    }

    public Extension<Subject> getExtension() {
        return extension;
    }

    public void setExtension(Extension<Subject> extension) {
        this.extension = extension;
    }

    public Subject project(Project project) {
        setProject(project);
        return this;
    }

    public Subject label(String label) {
        setLabel(label);
        return this;
    }

    public Subject randomLabel() {
        return label(RandomUtils.randomID());
    }

    public Subject gender(Gender gender) {
        setGender(gender);
        return this;
    }

    public Subject yob(int yob) {
        setYob(yob);
        return this;
    }

    public Subject age(int age) {
        setAge(age);
        return this;
    }

    public Subject dob(String dob) {
        setDob(dob);
        return this;
    }

    public Subject handedness(Handedness handedness) {
        setHandedness(handedness);
        return this;
    }

    public Subject race(String race) {
        setRace(race);
        return this;
    }

    public Subject ethnicity(String ethnicity) {
        setEthnicity(ethnicity);
        return this;
    }

    public Subject height(int height) {
        setHeight(height);
        return this;
    }

    public Subject weight(int weight) {
        setWeight(weight);
        return this;
    }

    public Subject src(String src) {
        setSrc(src);
        return this;
    }

    public Subject education(int education) {
        setEducation(education);
        return this;
    }

    public Subject shares(List<Share> shares) {
        setShares(shares);
        return this;
    }

    public Subject addShare(Share share) {
        shares.add(share);
        return this;
    }

    public Subject resources(List<Resource> resources) {
        setResources(resources);
        return this;
    }

    public Subject addResource(Resource resource) {
        resources.add(resource);
        return this;
    }

    public Subject experiments(List<SubjectAssessor> experiments) {
        setExperiments(experiments);
        return this;
    }

    public Subject addExperiment(SubjectAssessor experiment) {
        if (!experiments.contains(experiment)) experiments.add(experiment);
        return this;
    }

    public Subject extension(Extension<Subject> extension) {
        setExtension(extension);
        return this;
    }

    public Subject group(String group) {
        setGroup(group);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject that = (Subject) o;

        if (project != null ? !project.equals(that.project) : that.project != null) return false;
        return label != null ? label.equals(that.label) : that.label == null;
    }

    @Override
    public int hashCode() {
        int result = project != null ? project.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return label;
    }

}
