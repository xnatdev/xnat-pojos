package org.nrg.xnat.pojo.dicom;

public class DicomObjectIdentifier {

    public static final DicomObjectIdentifier DEFAULT = new DicomObjectIdentifier("dicomObjectIdentifier", "Default DICOM object identifier (ClassicDicomObjectIdentifier)");

    private String id;
    private String displayName;

    public DicomObjectIdentifier(String id, String displayName) {
        setId(id);
        setDisplayName(displayName);
    }

    public DicomObjectIdentifier() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DicomObjectIdentifier that = (DicomObjectIdentifier) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return displayName != null ? displayName.equals(that.displayName) : that.displayName == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
        return result;
    }
}