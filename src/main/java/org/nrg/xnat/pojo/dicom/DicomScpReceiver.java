package org.nrg.xnat.pojo.dicom;

public class DicomScpReceiver {

    private String aeTitle;
    private int port;
    private boolean enabled = true;
    private String host;
    private DicomObjectIdentifier identifier = DicomObjectIdentifier.DEFAULT;

    public DicomScpReceiver(String aeTitle, int port, boolean enabled, String host, DicomObjectIdentifier identifier) {
        this.aeTitle = aeTitle;
        this.port = port;
        this.enabled = enabled;
        this.host = host;
        this.identifier = identifier;
    }

    public DicomScpReceiver() {}

    public String getAeTitle() {
        return aeTitle;
    }

    public void setAeTitle(String aeTitle) {
        this.aeTitle = aeTitle;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public DicomObjectIdentifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(DicomObjectIdentifier identifier) {
        this.identifier = identifier;
    }

    public boolean usesDefaultIdentifier() {
        return DicomObjectIdentifier.DEFAULT.equals(identifier);
    }

    public DicomScpReceiver aeTitle(String aeTitle) {
        setAeTitle(aeTitle);
        return this;
    }

    public DicomScpReceiver port(int port) {
        setPort(port);
        return this;
    }

    public DicomScpReceiver enabled(boolean enabled) {
        setEnabled(enabled);
        return this;
    }

    public DicomScpReceiver host(String host) {
        setHost(host);
        return this;
    }

    public DicomScpReceiver identifier(DicomObjectIdentifier identifier) {
        setIdentifier(identifier);
        return this;
    }

    public String toString() {
        return String.format("(aeTitle: %s, port: %d, enabled: %b, host: %s%s)", aeTitle, port, enabled, host, (!usesDefaultIdentifier()) ? ", identifier: " + identifier.getId() : "");
    }
}
