package org.nrg.xnat.pojo.custom_variable;

import org.nrg.xnat.pojo.DataType;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.util.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomVariableSet {

    private List<Project> projects = new ArrayList<>();
    private DataType dataType;
    private boolean isProjectSpecific;
    private String name;
    private String description;
    private final List<CustomVariable> customVariables = new ArrayList<>();

    public CustomVariableSet() {}

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        ListUtils.copyInto(projects, this.projects);
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public boolean isProjectSpecific() {
        return isProjectSpecific;
    }

    public void setProjectSpecific(boolean projectSpecific) {
        isProjectSpecific = projectSpecific;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CustomVariable> getCustomVariables() {
        return customVariables;
    }

    public void setCustomVariables(List<CustomVariable> customVariables) {
        ListUtils.copyInto(customVariables, this.customVariables);
    }

    public CustomVariableSet projects(List<Project> projects) {
        setProjects(projects);
        return this;
    }

    public CustomVariableSet dataType(DataType dataType) {
        setDataType(dataType);
        return this;
    }

    public CustomVariableSet projectSpecific(boolean specific) {
        setProjectSpecific(specific);
        return this;
    }

    public CustomVariableSet name(String name) {
        setName(name);
        return this;
    }

    public CustomVariableSet description(String description) {
        setDescription(description);
        return this;
    }

    public CustomVariableSet customVariables(List<CustomVariable> customVariables) {
        setCustomVariables(customVariables);
        return this;
    }

}
