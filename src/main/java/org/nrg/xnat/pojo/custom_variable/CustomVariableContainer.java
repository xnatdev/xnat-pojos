package org.nrg.xnat.pojo.custom_variable;

import org.nrg.xnat.util.MapUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class CustomVariableContainer {

    private final Map<CustomVariableSet, Map<CustomVariable, Object>> customVariables = new HashMap<>();

    public Map<CustomVariableSet, Map<CustomVariable, Object>> getCustomVariables() {
        return customVariables;
    }

    public void setCustomVariables(Map<CustomVariableSet, Map<CustomVariable, Object>> customVariables) {
        MapUtils.copyInto(customVariables, this.customVariables);
    }

    public void addCustomVariableSet(CustomVariableSet variableSet, Map<CustomVariable, Object> variables) {
        customVariables.put(variableSet, variables);
    }

}
