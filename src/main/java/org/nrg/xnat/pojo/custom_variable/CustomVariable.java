package org.nrg.xnat.pojo.custom_variable;

import org.nrg.xnat.util.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomVariable<T> {

    private String name;
    private boolean required = false;
    private final List<T> possibleValues = new ArrayList<>();

    public CustomVariable(String name) {
        this.name = name;
    }

    public CustomVariable() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<T> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<T> possibleValues) {
        ListUtils.copyInto(possibleValues, this.possibleValues);
    }

    public CustomVariable name(String name) {
        setName(name);
        return this;
    }

    public CustomVariable required(boolean required) {
        setRequired(required);
        return this;
    }

    public CustomVariable possibleValues(List<T> possibleValues) {
        ListUtils.copyInto(possibleValues, this.possibleValues);
        return this;
    }

}
