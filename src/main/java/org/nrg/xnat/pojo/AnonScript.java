package org.nrg.xnat.pojo;

import org.nrg.xnat.Extension;
import org.nrg.xnat.enums.DicomEditVersion;
import org.nrg.xnat.pojo.extensions.AnonScriptExtension;

public class AnonScript {

    private String contents;
    private DicomEditVersion version;
    private AnonScriptExtension extension;

    public AnonScript(String contents, DicomEditVersion version) {
        this.contents = contents;
        this.version = version;
    }

    public AnonScript() {}

    public String getContents() {
        if (contents != null) return contents; // cache contents

        if (extension == null) {
            throw new RuntimeException("You must either provide contents for an anon script, or extend AnonScriptExtension.class and provide a way to read a script in.");
        }
        contents = extension.readScript();
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public DicomEditVersion getVersion() {
        return version;
    }

    public void setVersion(DicomEditVersion version) {
        this.version = version;
    }

    public Extension<AnonScript> getExtension() {
        return extension;
    }

    public void setExtension(AnonScriptExtension extension) {
        this.extension = extension;
    }

    public AnonScript contents(String contents) {
        setContents(contents);
        return this;
    }

    public AnonScript version(DicomEditVersion version) {
        setVersion(version);
        return this;
    }

    public AnonScript extension(AnonScriptExtension extension) {
        setExtension(extension);
        return this;
    }

}
