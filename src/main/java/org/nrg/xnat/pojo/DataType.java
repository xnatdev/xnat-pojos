package org.nrg.xnat.pojo;

public class DataType {

    public static final DataType MR_SESSION = new DataType("mr");
    public static final DataType PET_SESSION = new DataType("pet");
    public static final DataType CT_SESSION = new DataType("ct");
    public static final DataType CR_SESSION = new DataType("cr");
    public static final DataType PET_MR_SESSION = new DataType("xnat:petmrSessionData", "PETMR", "PET MR Session", "PET MR Sessions");

    public static final DataType PROJECT = new DataType("xnat:projectData", null, "Project", "Projects");
    public static final DataType SUBJECT = new DataType("xnat:subjectData", null, "Subject", "Subjects");

    private String xsiType;
    private String code;
    private String singularName;
    private String pluralName;

    public DataType(String xsiType, String code, String singularName, String pluralName) {
        this.xsiType = xsiType;
        this.code = code;
        this.singularName = singularName;
        this.pluralName = pluralName;
    }

    public DataType(String sessionCode) {
        this(String.format("xnat:%sSessionData", sessionCode.toLowerCase()), sessionCode.toUpperCase(), String.format("%s Session", sessionCode.toUpperCase()), String.format("%s Sessions", sessionCode.toUpperCase()));
    }

    public DataType() {}

    public String getXsiType() {
        return xsiType;
    }

    public void setXsiType(String xsiType) {
        this.xsiType = xsiType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSingularName() {
        return singularName;
    }

    public void setSingularName(String singularName) {
        this.singularName = singularName;
    }

    public String getPluralName() {
        return (pluralName == null) ? singularName + "s" : pluralName;
    }

    public void setPluralName(String pluralName) {
        this.pluralName = pluralName;
    }

    public DataType xsiType(String xsiType) {
        setXsiType(xsiType);
        return this;
    }

    public DataType code(String code) {
        setCode(code);
        return this;
    }

    public DataType singularName(String singularName) {
        setSingularName(singularName);
        return this;
    }

    public DataType pluralName(String pluralName) {
        setPluralName(pluralName);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataType dataType = (DataType) o;

        if (xsiType != null ? !xsiType.equals(dataType.xsiType) : dataType.xsiType != null) return false;
        if (code != null ? !code.equals(dataType.code) : dataType.code != null) return false;
        if (singularName != null ? !singularName.equals(dataType.singularName) : dataType.singularName != null)
            return false;
        return pluralName != null ? pluralName.equals(dataType.pluralName) : dataType.pluralName == null;
    }

    @Override
    public int hashCode() {
        int result = xsiType != null ? xsiType.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (singularName != null ? singularName.hashCode() : 0);
        result = 31 * result + (pluralName != null ? pluralName.hashCode() : 0);
        return result;
    }
}
