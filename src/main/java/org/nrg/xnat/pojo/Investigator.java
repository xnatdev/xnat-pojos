package org.nrg.xnat.pojo;

import org.nrg.xnat.Extension;

public class Investigator {
    private String title;
    private String firstname;
    private String lastname;
    private String institution;
    private String department;
    private String email;
    private String phone;
    private int xnatInvestigatordataId;
    private Extension<Investigator> extension;

    public Investigator() {}

    public String getFullName() {
        return (firstname != null && lastname != null) ? String.format("%s, %s", lastname, firstname) : null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getXnatInvestigatordataId() {
        return xnatInvestigatordataId;
    }

    public void setXnatInvestigatordataId(int xnatInvestigatordataId) {
        this.xnatInvestigatordataId = xnatInvestigatordataId;
    }

    public void setXnatInvestigatordataId(String xnatInvestigatordataId) {
        this.xnatInvestigatordataId = Integer.parseInt(xnatInvestigatordataId);
    }

    public Extension<Investigator> getExtension() {
        return extension;
    }

    public void setExtension(Extension<Investigator> extension) {
        this.extension = extension;
    }

    public Investigator title(String title) {
        setTitle(title);
        return this;
    }

    public Investigator firstname(String firstname) {
        setFirstname(firstname);
        return this;
    }

    public Investigator lastname(String lastname) {
        setLastname(lastname);
        return this;
    }

    public Investigator institution(String institution) {
        setInstitution(institution);
        return this;
    }

    public Investigator department(String department) {
        setDepartment(department);
        return this;
    }

    public Investigator phone(String phone) {
        setPhone(phone);
        return this;
    }

    public Investigator email(String email) {
        setEmail(email);
        return this;
    }

    public Investigator id(int id) {
        setXnatInvestigatordataId(id);
        return this;
    }

    public Investigator extension(Extension<Investigator> extension) {
        setExtension(extension);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Investigator that = (Investigator) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (firstname != null ? !firstname.equals(that.firstname) : that.firstname != null) return false;
        return lastname != null ? lastname.equals(that.lastname) : that.lastname == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        return result;
    }
}
