package org.nrg.xnat.pojo;

import org.nrg.xnat.Extension;
import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.pojo.resources.Resource;
import org.nrg.xnat.util.ListUtils;
import org.nrg.xnat.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;

public class Project {

    private String id;
    private String runningTitle;
    private String title;
    private String description;
    private final List<String> keywords = new ArrayList<>();
    private final List<String> aliases = new ArrayList<>();
    private Investigator pi;
    private final List<Investigator> investigators = new ArrayList<>();
    private final List<User> owners = new ArrayList<>();
    private final List<User> members = new ArrayList<>();
    private final List<User> collaborators = new ArrayList<>();
    private Accessibility accessibility = Accessibility.PRIVATE;
    private final List<Subject> subjects = new ArrayList<>();
    private final List<Subject> secondarySubjects = new ArrayList<>();
    private final List<Resource> projectResources = new ArrayList<>();
    private Extension<Project> extension;

    public Project(String id) {
        setId(id);
    }

    public Project() {
        this(RandomUtils.randomID());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRunningTitle() {
        return (runningTitle != null) ? runningTitle : id;
    }

    public void setRunningTitle(String runningTitle) {
        this.runningTitle = runningTitle;
    }

    public String getTitle() {
        return (title != null) ? title : id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        ListUtils.copyInto(keywords, this.keywords);
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        ListUtils.copyInto(aliases, this.aliases);
    }

    public Investigator getPi() {
        return pi;
    }

    public void setPi(Investigator pi) {
        this.pi = pi;
    }

    public List<Investigator> getInvestigators() {
        return investigators;
    }

    public void setInvestigators(List<Investigator> investigators) {
        ListUtils.copyInto(investigators, this.investigators);
    }

    public List<User> getOwners() {
        return owners;
    }

    public void setOwners(List<User> owners) {
        ListUtils.copyInto(owners, this.owners);
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        ListUtils.copyInto(members, this.members);
    }

    public List<User> getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(List<User> collaborators) {
        ListUtils.copyInto(collaborators, this.collaborators);
    }

    public Accessibility getAccessibility() {
        return accessibility;
    }

    public void setAccessibility(Accessibility accessibility) {
        this.accessibility = accessibility;
    }

    public void setAccessibility(String accessibility) {
        setAccessibility(Accessibility.get(accessibility));
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        ListUtils.copyInto(subjects, this.subjects);
    }

    public List<Subject> getSecondarySubjects() {
        return secondarySubjects;
    }

    public void setSecondarySubjects(List<Subject> secondarySubjects) {
        ListUtils.copyInto(secondarySubjects, this.secondarySubjects);
    }

    public List<Resource> getProjectResources() {
        return projectResources;
    }

    public void setProjectResources(List<Resource> projectResources) {
        ListUtils.copyInto(projectResources, this.projectResources);
    }

    public Extension<Project> getExtension() {
        return extension;
    }

    public void setExtension(Extension<Project> extension) {
        this.extension = extension;
    }

    public Project id(String id) {
        setId(id);
        return this;
    }

    public Project randomId() {
        return id(RandomUtils.randomID());
    }

    public Project runningTitle(String runningTitle) {
        setRunningTitle(runningTitle);
        return this;
    }

    public Project title(String title) {
        setTitle(title);
        return this;
    }

    public Project description(String description) {
        setDescription(description);
        return this;
    }

    public Project keywords(List<String> keywords) {
        setKeywords(keywords);
        return this;
    }

    public Project addKeyword(String keyword) {
        keywords.add(keyword);
        return this;
    }

    public Project aliases(List<String> aliases) {
        setAliases(aliases);
        return this;
    }

    public Project addAlias(String alias) {
        aliases.add(alias);
        return this;
    }

    public Project pi(Investigator pi) {
        setPi(pi);
        return this;
    }

    public Project investigators(List<Investigator> investigators) {
        setInvestigators(investigators);
        return this;
    }

    public Project owners(List<User> owners) {
        setOwners(owners);
        return this;
    }

    public Project addOwner(User owner) {
        owners.add(owner);
        return this;
    }

    public Project members(List<User> members) {
        setMembers(members);
        return this;
    }

    public Project addMember(User member) {
        members.add(member);
        return this;
    }

    public Project collaborators(List<User> collaborators) {
        setCollaborators(collaborators);
        return this;
    }

    public Project addCollaborator(User collaborator) {
        collaborators.add(collaborator);
        return this;
    }

    public Project accessibility(Accessibility accessibility) {
        setAccessibility(accessibility);
        return this;
    }

    public Project accessibility(String accessibility) {
        setAccessibility(accessibility);
        return this;
    }

    public Project subjects(List<Subject> subjects) {
        setSubjects(subjects);
        return this;
    }

    public Project secondarySubjects(List<Subject> subjects) {
        setSecondarySubjects(subjects);
        return this;
    }

    public Project addSubject(Subject subject) {
        if (!subjects.contains(subject)) subjects.add(subject);
        return this;
    }

    public Project resources(List<Resource> resources) {
        setProjectResources(resources);
        return this;
    }

    public Project addResource(Resource resource) {
        projectResources.add(resource);
        return this;
    }

    public Project extension(Extension<Project> extension) {
        setExtension(extension);
        return this;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        return id != null ? id.equals(project.id) : project.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
