package org.nrg.xnat.jackson.modules

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.xnat.jackson.deserializers.*
import org.nrg.xnat.pojo.Investigator
import org.nrg.xnat.pojo.Project
import org.nrg.xnat.pojo.Subject
import org.nrg.xnat.pojo.experiments.Scan

class XnatRestReadDeserializationModule {

    static SimpleModule build() {
        final SimpleModule module = new SimpleModule("XNAT_REST_Deserializers")

        module.addDeserializer(Investigator, new InvestigatorDeserializer())
        module.addDeserializer(Project, new ProjectDeserializer())
        module.addDeserializer(Subject, new SubjectDeserializer())
        module.addDeserializer(Scan, new ScanDeserializer())

        module
    }

}
