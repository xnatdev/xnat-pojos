package org.nrg.xnat.jackson.modules

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.xnat.jackson.serializers.*
import org.nrg.xnat.pojo.Investigator
import org.nrg.xnat.pojo.Project
import org.nrg.xnat.pojo.Subject
import org.nrg.xnat.pojo.User
import org.nrg.xnat.pojo.experiments.Scan
import org.nrg.xnat.pojo.resources.ResourceFile

class XnatRestWriteSerializationModule {

    static SimpleModule build() {
        final SimpleModule module = new SimpleModule("XNAT_REST_Serializers")

        module.addSerializer(Project, new ProjectSerializer())
        module.addSerializer(Subject, new SubjectSerializer())
        module.addSerializer(Scan, new ScanSerializer())

        module.addSerializer(ResourceFile, new ResourceFileSerializer())

        module.addSerializer(User, new UserSerializer())
        module.addSerializer(Investigator, new InvestigatorSerializer())

        module
    }
}
