package org.nrg.xnat.jackson.deserializers

import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.pojo.experiments.Experiment

abstract class ExperimentDeserializer<T extends Experiment> extends CustomDeserializer<T> {

    protected void setCommonFields(JsonNode experimentNode, T experiment) {
        setStringIfNonnull(experimentNode, 'label', experiment.&setLabel)
    }

}
