package org.nrg.xnat.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.pojo.Subject

class SubjectSerializer extends CustomSerializer<Subject> {

    @Override
    void serialize(Subject value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject()

        writeStandardValues(gen, value)

        gen.writeEndObject()
    }

    protected void writeStandardValues(JsonGenerator gen, Subject subject) {
        writeEnumToString(gen, 'gender', subject.gender)
        writeIntFieldIfNonzero(gen, 'yob', subject.yob)
        writeIntFieldIfNonzero(gen, 'age', subject.age)
        writeEnumToString(gen, 'handedness', subject.handedness)
        writeIntFieldIfNonzero(gen, 'education', subject.education)
        writeStringFieldIfNonempty(gen, 'race', subject.race)
        writeStringFieldIfNonempty(gen, 'ethnicity', subject.ethnicity)
        writeIntFieldIfNonzero(gen, 'height', subject.height)
        writeIntFieldIfNonzero(gen, 'weight', subject.weight)
        writeStringFieldIfNonempty(gen, 'src', subject.src)
    }

}
